# Lost in Translation - by Renze Stolte
Lost in translation is an internet page that translates text to american sign language through images. The React Framework and React Router were used to make this application. Link to heroku: https://lost-in-translation-renze.herokuapp.com/.

### Functionalities:

## Login page
Here, the user can enter a name that will be used to track their history. After having submitted their name, they will be taken to the translation page.

## Translation page
Here, the user can enter any text, which will then be translated to american sign language. Each letter is translated seperately and showed as an image of a hand doing the corresponding sign. If the user want, they can press their name in the top right to be taken to the profile page. Alternatively, they can press 'Lost in Translation' in the top left to return to the login page.

## Profile page
The profile page displays the ten last translation of the currently logged in user. A button will clear the user's history, logg them out, and return to the login page.


