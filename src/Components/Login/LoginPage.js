import Header from "./Header";
import InputBar from "../Input/InputBar";
import './LoginPage.css'
import {useState} from "react";
import {Redirect} from "react-router-dom";
import NavigationBar from "../NavigationBar/NavigationBar";

function LoginPage() {

    const [userName, setUserName] = useState('')

    const handleNameSubmission = (submittedName) => {
        setUserName(submittedName)
    }

    return (
        <>
            {userName && <Redirect to={'/translate/' + userName}/>}
            <NavigationBar loginName=""/>
            <div className='login-page'>
                <Header/>
                <InputBar submitInput={handleNameSubmission} placeHolder='What is your name?'/>
            </div>
        </>
    )
}

export default LoginPage