import './Header.css'

function Header() {
    return (
        <div className="header">
            <div>
                <img src={process.env.PUBLIC_URL + '/Logo.png'} className='header-logo' alt='Happy robot logo'/>
            </div>
            <div>
                <h1>Lost in translation</h1>
                <h3>Get started</h3>
            </div>
        </div>
    )
}

export default Header;