import NavigationBar from "../NavigationBar/NavigationBar";
import {useHistory} from "react-router-dom";
import './ProfilePage.css'

const ProfilePage = props => {
    const {match: {params}} = props
    const userName = params.id.replace(':', '')
    const myStorage = window.localStorage
    let history = useHistory();

    let translatedWords = JSON.parse(myStorage.getItem(userName))
    if (translatedWords === null) {
        translatedWords = []
    }

    function clearUserAndLogOut() {
        myStorage.removeItem(userName)
        history.push('/')
    }

    return (
        <>
            <NavigationBar loginName={userName}/>
            <div className='search-header'>
                <div>
                    <img src={process.env.PUBLIC_URL + '/Logo-Hello.png'} className='waving-logo'
                         alt='Happy robot logo'/>
                </div>
                <div>
                    <h1>History</h1>
                    <h3>Displaying your 10 last translations</h3>
                </div>
            </div>
            <div className='content-display'>
                <ul className="list">
                    {translatedWords.map((name, index) => (
                        <li key={index}>{name}</li>
                    ))}
                </ul>
                <button onClick={clearUserAndLogOut}>Clear history and logout</button>
                <i className="fas fa-arrow-right"></i>
            </div>

        </>
    )
}

export default ProfilePage
