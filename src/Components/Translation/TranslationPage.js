import InputBar from "../Input/InputBar";
import './TranslationPage.css'
import SignDisplayer from "./SignDisplayer";
import {useState} from "react";
import NavigationBar from "../NavigationBar/NavigationBar";
import {useParams} from "react-router";

const TranslationPage = () => {
    const [wordToTranslate, setWordToTranslate] = useState('')
    const {id} = useParams()
    const myStorage = window.localStorage
    let searchHistory = []

    // Get the search history if it exist in the local storage
    if (myStorage.getItem(id)) {
        searchHistory = JSON.parse(myStorage.getItem(id))
    }

    // Triggers when new translation is inputted
    const handleInputSubmission = (input) => {
        if (input == null)
            return
        setWordToTranslate(input)
        if (searchHistory.length >= 10) {
            // Remove the first translation in the search history
            searchHistory.reverse().pop()
            searchHistory.reverse()
        }
        searchHistory.push(input)
        myStorage.setItem(id, JSON.stringify(searchHistory))
    }

    return (
        <>
            <NavigationBar loginName={id}/>
            <div className='main-page'>
                <InputBar submitInput={handleInputSubmission} placeHolder='Input text to translate'/>
                <div className={'sign-display'}>
                    <SignDisplayer wordToTranslate={wordToTranslate}/>
                </div>
            </div>
        </>
    )
}

export default TranslationPage