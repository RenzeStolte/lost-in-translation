const SingleSign = (prop) => {

    const getImage = (letter) => {
        // If letter not a normal character, set letter to '-' so it displays an empty box
        if (!letter.match(/[a-z]/i))
            letter = '-'
        return {
            id: letter,
            src: process.env.PUBLIC_URL + `/individial_signs/${letter}.png`,
            title: letter,
            description: letter + ' in sign language'
        }
    }

    const image = getImage(prop.signValue)

    return (
        <div className="sign-image">
            <img key={image.id}
                 src={image.src}
                 title={image.title}
                 alt={image.description}
            />
        </div>
    )
}

export default SingleSign