import './SignDisplayer.css'
import SingleSign from "./SingleSign";

const SignDisplayer = (props) => {
    let characters = []
    if (props.wordToTranslate !== null)
        characters = [...props.wordToTranslate]

    return (
        <div className="sign-box">
            {characters.map((character, index) =>
                <SingleSign signValue={character.toLowerCase()} key={index}/>
            )}
        </div>
    )
}

export default SignDisplayer