import './NavigationBar.css';
import {NavLink} from "react-router-dom";

const NavigationBar = (props) => {

    return (
        <div className="nav-bar">
            <NavLink to={'/'} className="home link-bar">
                Lost in translation
            </NavLink>
            <NavLink to={'/profile/' + props.loginName} className="user-name link-bar">
                {props.loginName}
            </NavLink>
        </div>
    )
}

export default NavigationBar;