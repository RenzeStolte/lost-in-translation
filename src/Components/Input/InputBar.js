import './InputBar.css'
import {useState} from "react";

const InputBar = (props) => {
    const [userInput, setUserInput] = useState()

    // Change userInput when user types
    const handleChangeOfInput = event => {
        setUserInput(event.target.value.trim())
    }

    // Submit userInput to parent
    const onSubmitInput = () => {
        props.submitInput(userInput)
    }

    return (
        <div className='input-bar'>
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
                  rel="stylesheet"/>
            <input type='text' onChange={handleChangeOfInput} placeholder={props.placeHolder}></input>
            <button onClick={onSubmitInput}><span className="material-icons">arrow_forward</span></button>
        </div>
    )
}

export default InputBar;