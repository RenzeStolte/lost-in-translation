import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import LoginPage from "./Components/Login/LoginPage";
import TranslationPage from "./Components/Translation/TranslationPage";
import ProfilePage from "./Components/Profile/ProfilePage";

function App() {

    return (
        <Router>
            <div className="App">
                <Switch>
                    <Route exact path='/' component={LoginPage}/>
                    <Route path='/translate/:id' component={TranslationPage}/>
                    <Route path='/profile/:id' component={ProfilePage}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
